use std::path::Path;

use sqlite;

fn main() {
    let newsboat_path = Path::new(env!("HOME")).join(".newsboat/cache.db");
    let pathstr = newsboat_path.clone();
    let connection = sqlite::open(newsboat_path)
        .expect(&format!("Failed to open newsboat database ({})", pathstr.to_string_lossy()));

    let mut statement = connection
        .prepare(r"select rss_feed.title, rss_item.title, rss_item.url
            FROM rss_item LEFT JOIN rss_feed ON rss_item.feedurl = rss_feed.rssurl
            WHERE unread = 1
            ORDER BY rssurl
        ").expect("Failed to prepare SQL statement");

    let mut last_title = "".to_string();
    while let sqlite::State::Row = statement.next().expect("Failed to read row") {
        let feed_title = statement.read::<String>(0).unwrap();
        let title = statement.read::<String>(1).unwrap();
        let url = statement.read::<String>(2).unwrap();

        if feed_title != last_title {
            println!("<h1>{}</h1>", feed_title);
            last_title = feed_title;
        }

        let url = url.replace("old.reddit.com", "i.reddit.com");
        println!(r#"<a href="{}">{}</a><br>"#, url, title)
    }
}
