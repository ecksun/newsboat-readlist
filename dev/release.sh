#!/bin/bash

tag="${tag:-"$(git describe)"}"
artifact="${1:?Need an artifact to upload}"
description="$(git show "$tag" --format="" --quiet | tail -n +4)"

project="ecksun/newsboat-readlist"
project_id=$(sed 's#/#%2F#' <<<"$project" )
project_url="https://gitlab.com/api/v4/projects/$project_id"

trap 'rm -rf $tmpdir' EXIT
tmpdir="$(mktemp -d)"

status=$(curl --request POST \
    --header "PRIVATE-TOKEN: $(cat ~/.config/gitlab/token)" \
    --form "file=@$artifact" \
    --output "${tmpdir}/upload" \
    --write-out "%{http_code}" \
    "$project_url/uploads"
    )

if [[ ${status} -lt 200 ]] || [[ ${status} -ge 300 ]]; then
    echo >&2 "Failed to upload artifact, got ${status}:"
    cat >&2 "$tmpdir/upload"
    exit 1
fi

jq >&2 . < "$tmpdir/upload"

release_data=$(jq --null-input \
    --arg project_id "$project_id" \
    --arg tag "$tag" \
    --arg artifact_name "$(basename "$artifact")" \
    --arg artifact_url "https://gitlab.com$(jq --raw-output .full_path < "$tmpdir/upload")" \
    --arg description "$description" \
    '
{
    id: $project_id,
    name: $tag,
    tag_name: $tag,
    description: $description,
    assets: {
        links: [{
                name: $artifact_name,
                url: $artifact_url,
                link_type:"other"
            }
        ]
    }
}'
)

status=$(curl \
    --header 'Content-Type: application/json' \
    --header "PRIVATE-TOKEN: $(cat ~/.config/gitlab/token)" \
    --data "@-" \
    --output "${tmpdir}/release" \
    --write-out "%{http_code}" \
     "$project_url/releases" <<<"$release_data"
     )

if [[ ${status} -lt 200 ]] || [[ ${status} -ge 300 ]]; then
    echo >&2 "Failed to create release, got ${status}:"
    cat >&2 "$tmpdir/release"
    exit 1
fi

jq >&2 . < "$tmpdir/release"
