#!/bin/bash

project="ecksun/newsboat-readlist"
project_id=$(sed 's#/#%2F#' <<<"$project" )
project_url="https://gitlab.com/api/v4/projects/$project_id"

trap 'rm -rf $tmpdir' EXIT
tmpdir="$(mktemp -d)"

curl \
    -X DELETE \
    --header 'Content-Type: application/json' \
    --header "PRIVATE-TOKEN: $(cat ~/.config/gitlab/token)" \
    "$project_url/releases/${1:?Need a release (tag) to delete}"
