name=newsboat-html
version=$(shell git describe)
version_short=$(patsubst v%,%,$(version))
deb_package_dir=out/$(name)-$(version)
deb_package=$(deb_package_dir).deb

.PHONY: release
release: $(deb_package)
	./dev/release.sh $(deb_package)

out/index.html: out/newsboat.html | out
	cat $^ > $@

.PHONY: upload
upload: out/index.html
	scp $< balder:public_html/reading.html

out/newsboat.html: ~/.newsboat/cache.db | out
	(cd ru && cargo run) > $@

ru/target/release/newsboat-html: $(shell find ru/src -type f) ru/Cargo.lock ru/Cargo.toml
	(cd ru && cargo build --release)

out:
	mkdir -p $@

.PHONY: deb
deb: $(deb_package)
$(deb_package): $(deb_package_dir)/
	fakeroot dpkg-deb --build "$(deb_package_dir)"

$(deb_package_dir)/: \
	$(deb_package_dir)/DEBIAN/ \
	$(deb_package_dir)/usr/bin/$(name) \

	@touch "$@"

$(deb_package_dir)/DEBIAN/: \
	$(deb_package_dir)/DEBIAN/conffile \
	$(deb_package_dir)/DEBIAN/control \

	@touch "$@"

$(deb_package_dir)/DEBIAN/control: debian/control
	(cat debian/control && printf "Version: %s\n" "$(version_short)") > "$@"

$(deb_package_dir)/DEBIAN/%: debian/%
	@mkdir -p "$(dir $@)"
	cp -p "debian/$*" "$@"

$(deb_package_dir)/usr/bin/$(name): ru/target/release/newsboat-html
	@mkdir -p "$(dir $@)"
	cp "$<" "$@"
